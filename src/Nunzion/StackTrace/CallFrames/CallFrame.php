<?php

namespace Nunzion\StackTrace\CallFrames;

use Nunzion\StackTrace\Source;
use ReflectionFunctionAbstract;

/**
 * @see https://bugs.php.net/bug.php?id=44428 #44428: "file" and "line" missing
 * in debug_backtrace() output
 */
abstract class CallFrame
{
    /**
     * @var array
     */
    private $args;

    /**
     * @var int
     */
    private $sourceLine;

    /**
     * @var Source|null
     */
    private $source;

    /**
     * @var array|null
     */
    private $namedArgs = null;

    /**
     * @var string|null
     */
    private $callingLine = null;

    /**
     * @var array|null
     */
    private $callingLineTokens = null;

    /**
     * @param array       $args
     * @param int         $line
     * @param Source|null $source
     */
    public function __construct(array $args = array(), $line = 0, Source $source = null)
    {
        $this->sourceLine = $line;
        $this->source = $source;
        $this->args = \array_values($args);
    }

    /**
     * @param int $pos
     * @return mixed
     */
    public function getArgument($pos)
    {
        $args = $this->getArguments();

        return $args[$pos];
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->args;
    }

    /**
     * @return bool
     */
    public function hasArgumentNames()
    {
        return (bool) \count(\array_filter(\array_keys($this->getArgumentsByName()), 'is_string'));
    }

    /**
     * @return array<string,mixed>
     */
    public function getArgumentsByName()
    {
        if ($this->namedArgs !== null)
            return $this->namedArgs;

        $namesAndDefaults = $this->getArgumentNamesAndDefaults();
        $curArg = \reset($this->args);
        $curDef = \reset($namesAndDefaults);
        $curName = \key($namesAndDefaults);
        $this->namedArgs = array();
        $newAdditionalArgs = array();

        $max = \count(\max($this->args, $namesAndDefaults));

        for ($i = 0; $i < $max; $i++)
        {
            if ($curName === "...") // ignore php internal name
                $curName = null;

            if ((\key($this->args) !== null) && ($curName !== null)) // assign value to name
                $this->namedArgs[$curName] = $curArg;
            else if (\key($this->args) !== null) // no name assigned
                $newAdditionalArgs[] = $curArg;
            else if ($curName !== null) // no value assigned
                $this->namedArgs[$curName] = $curDef;

            if (\key($this->args) !== null)
                $curArg = \next($this->args);
            if (\key($namesAndDefaults) !== null)
            {
                $curDef = \next($namesAndDefaults);
                $curName = \key($namesAndDefaults);
            }
        }

        if ($newAdditionalArgs !== array())
            $this->namedArgs["..."] = $newAdditionalArgs;

        return $this->namedArgs;
    }

    /**
     * @return array<string,mixed>
     */
    protected function getArgumentNamesAndDefaults()
    {
        $reflectionFunc = $this->getTargetReflectionFunction();
        $result = array();
        foreach ($reflectionFunc->getParameters() as $param)
            $result[($param->getName() !== "...") ? '$' . $param->getName() : $param->getName()] =
                $param->isDefaultValueAvailable() ? $param->getDefaultValue()
                    : null;

        return $result;
    }

    /**
     * @return ReflectionFunctionAbstract|null
     */
    public abstract function getTargetReflectionFunction();

    /**
     * @param string $name
     * @return bool
     */
    public function hasArgumentName($name)
    {
        $args = $this->getArgumentsByName();

        return isset($args[$name]);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getArgumentByName($name)
    {
        $args = $this->getArgumentsByName();

        return $args[$name];
    }

    /**
     * Gets the line. The first line is line 1.
     *
     * @return int|null
     */
    public function getSourceLine()
    {
        return ($this->hasSourceLine()) ? $this->sourceLine : null;
    }

    /**
     * @return bool
     */
    public function hasSourceLine()
    {
        return $this->sourceLine > 0;
    }

    /**
     * @return bool
     */
    public function hasSource()
    {
        return $this->source !== null;
    }

    /**
     * @return Source|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return array|null
     */
    protected function getCallingLineTokens()
    {
        if ($this->callingLineTokens === null)
        {
            $tokens = \token_get_all($this->source->getContent());
            $start = 0;
            while (isset($tokens[$start]) && (\is_string($tokens[$start]) ||
                                              (\is_array($tokens[$start]) && ($tokens[$start][2] < $this->sourceLine))))
                $start++;
            if (!isset($tokens[$start]))
                return null;
            $end = $start + 1;
            while (isset($tokens[$end]) && (\is_string($tokens[$end]) ||
                                            (\is_array($tokens[$end]) && ($tokens[$end][2] === $this->sourceLine))))
                $end++;
            if (!isset($tokens[$end]))
                $end--;
            $this->callingLineTokens = \array_slice($tokens, $start, $end - $start);
        }

        return $this->callingLineTokens;
    }

    /**
     * @return string|null
     */
    public function getCallingLine()
    {
        if ($this->callingLine === null)
        {
            if ($this->hasSource())
            {
                $tokens = $this->getCallingLineTokens();
                if ($tokens !== null)
                {
                    foreach ($tokens as $key => $token)
                    {
                        if (\is_array($token))
                            $tokens[$key] = $token[1];
                    }
                    $this->callingLine = \trim(\implode($tokens));
                }
                else
                    $this->callingLine = "";
            }
            else
                $this->callingLine = "";
        }

        return ($this->callingLine !== "") ? $this->callingLine : null;
    }

    /**
     * @return string|null
     */
    /*public function getTrimmedCallingLine()
    {
        // TODO implement/move
    }*/
}
