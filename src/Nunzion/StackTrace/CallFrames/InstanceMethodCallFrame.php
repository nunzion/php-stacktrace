<?php

namespace Nunzion\StackTrace\CallFrames;

use Nunzion\StackTrace\Source;

class InstanceMethodCallFrame extends MethodCallFrame
{
    /**
     * @var object
     */
    protected $targetObject;

    /**
     * @param object      $object
     * @param string      $class
     * @param string      $methodName
     * @param array       $args
     * @param int         $line
     * @param Source|null $source
     */
    public function __construct($object, $class, $methodName, array $args, $line, Source $source = null)
    {
        parent::__construct($class, $methodName, $args, $line, $source);

        $this->targetObject = $object;
    }

    /**
     * @return object
     */
    public function getTargetObject()
    {
        return $this->targetObject;
    }
}
