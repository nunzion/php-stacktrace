<?php

namespace Nunzion\StackTrace\FilterCriteria;

use Nunzion\StackTrace\CallFrames\CallFrame;
use Nunzion\StackTrace\CallFrames\ClosureCallFrame;
use Nunzion\StackTrace\CallFrames\FunctionCallFrame;
use Nunzion\StackTrace\CallFrames\MethodCallFrame;

class NamespaceFilterCriterion extends FilterCriterion
{
    /**
     * @var string
     */
    private $namespace;

    /**
     * @var bool
     */
    private $meetSubNamespaces;

    /**
     * @param string $namespace
     * @return bool
     */
    private function meetsNamespace($namespace)
    {
        return $this->meetSubNamespaces ?
            \substr_compare($this->namespace, $namespace, 0, \strlen($this->namespace)) === 0 :
            $this->namespace === $namespace;
    }

    /**
     * @param string $namespace
     * @param bool   $meetSubNamespaces
     */
    public function __construct($namespace, $meetSubNamespaces = true)
    {
        $this->namespace = $namespace;
        $this->meetSubNamespaces = $meetSubNamespaces;
    }

    /**
     * @param CallFrame $callFrame
     * @return bool
     */
    public function meetsCallFrame(CallFrame $callFrame)
    {
        return (($callFrame instanceof ClosureCallFrame) &&
                $this->meetsNamespace($callFrame->getTargetClosureScope()->getNamespace()))
               || (($callFrame instanceof FunctionCallFrame) && ($callFrame->getTargetReflectionFunction() !== null) &&
                   $this->meetsNamespace($callFrame->getTargetReflectionFunction()->getNamespaceName()))
               || (($callFrame instanceof MethodCallFrame) &&
                   $this->meetsNamespace($callFrame->getTargetReflectionFunction()->getNamespaceName()));
    }

    /**
     * @return string
     */
    protected function getIdentifier()
    {
        return $this->namespace . ($this->meetSubNamespaces ? "*" : "");
    }
}
