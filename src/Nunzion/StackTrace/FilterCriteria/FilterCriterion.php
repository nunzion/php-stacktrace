<?php

namespace Nunzion\StackTrace\FilterCriteria;

use Nunzion\StackTrace\CallFrames\CallFrame;

abstract class FilterCriterion
{
    /**
     * @param FilterCriterion[] $criteria
     * @param CallFrame[]       $frames
     * @return array<string,int[]>
     */
    public static function checkCriteriaOnFrames($criteria, $frames)
    {
        $result = array();
        foreach ($frames as $frameKey => $frame)
        {
            foreach ($criteria as $criterion)
            {
                if (!$criterion->meetsCallFrame($frame))
                    continue;

                $criterionName = (string) $criterion;
                if (!isset($result[$criterionName]))
                    $result[$criterionName] = array();

                $result[$criterionName][] = $frameKey;
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    protected abstract function getIdentifier();

    /**
     * @param CallFrame $callFrame
     * @return bool
     */
    public abstract function meetsCallFrame(CallFrame $callFrame);

    /**
     * @return string
     */
    public function __toString()
    {
        return \get_class($this) . ":" . $this->getIdentifier();
    }
}
