<?php

namespace Nunzion\StackTrace\FilterCriteria;

use Nunzion\StackTrace\CallFrames\CallFrame;
use Nunzion\StackTrace\CallFrames\MethodCallFrame;

class ClassFilterCriterion extends FilterCriterion
{
    /**
     * @var string
     */
    private $class;

    /**
     * @param string $class
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    /**
     * @param CallFrame $callFrame
     * @return bool
     */
    public function meetsCallFrame(CallFrame $callFrame)
    {
        return ($callFrame instanceof MethodCallFrame) && ($callFrame->getTargetClassName() === $this->class);
    }

    /**
     * @return string
     */
    protected function getIdentifier()
    {
        return $this->class;
    }
}
