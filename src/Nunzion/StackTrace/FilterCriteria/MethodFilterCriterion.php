<?php

namespace Nunzion\StackTrace\FilterCriteria;

use Nunzion\StackTrace\CallFrames\CallFrame;
use Nunzion\StackTrace\CallFrames\InstanceMethodCallFrame;

class MethodFilterCriterion extends FilterCriterion
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var string|null
     */
    private $class;

    /**
     * @var bool|null
     */
    private $static;

    /**
     * @param string      $method
     * @param string|null $class
     * @param bool|null   $static
     */
    public function __construct($method, $class = null, $static = null)
    {
        $this->method = $method;
        $this->class = $class;
        $this->static = $static;
    }

    /**
     * @param CallFrame $callFrame
     * @return bool
     */
    public function meetsCallFrame(CallFrame $callFrame)
    {
        if ((($this->static !== true) && ($callFrame instanceof InstanceMethodCallFrame)) ||
            (($this->static !== false) && ($callFrame instanceof StaticMethodCallFrame))
        )
            return ($callFrame->getTargetMethodName() === $this->method) &&
                   (($this->class !== null) ? $this->class === $callFrame->getTargetClass() : true);
        else
            return false;
    }

    /**
     * @return string
     */
    protected function getIdentifier()
    {
        $pre = ($this->class !== null ? $this->class : "");
        if ($this->static === true)
            $pre .= "::";
        else if ($this->static === false)
            $pre .= "->";
        else
            $pre .= "##";

        return $pre . $this->method;
    }
}
