<?php

namespace Nunzion\StackTrace;

use Exception;
use Nunzion\StackTrace\CallFrames\CallFrame;
use Nunzion\StackTrace\CallFrames\ClosureCallFrame;
use Nunzion\StackTrace\CallFrames\FunctionCallFrame;
use Nunzion\StackTrace\CallFrames\InstanceMethodCallFrame;
use Nunzion\StackTrace\CallFrames\StaticMethodCallFrame;
use ReflectionFunction;
use ReflectionFunctionAbstract;
use ReflectionMethod;

class StackTrace
{
    /**
     * @var CallFrame[]
     */
    protected $callFrames;

    /**
     * @param CallFrame[] $callFrames
     * @throws Exception If now callFrames are given.
     */
    public function __construct(array $callFrames)
    {
        if (\count($callFrames) === 0)
            throw new Exception("Invalid StackTrace: Size 0");
        $this->callFrames = $callFrames;
    }

    /**
     * @param bool $useXdebug
     * @return StackTrace
     * @throws Exception
     */
    public static function getCurrent($useXdebug = true)
    {
        /* $trace elements:
         * function (string)  The current function name. See also __FUNCTION__.
         * line (integer)     The current line number. See also __LINE__.
         * file (string)      The current file name. See also __FILE__.
         * class (string)     The current class name. See also __CLASS__
         * object (object)    The current object.
         * type (string)      The current call type. If a method call, "->" is returned. If a static
         *                    method call, "::" is returned. If a function call, nothing is returned.
         * args (array)       If inside a function, this lists the functions arguments. If
         *                    inside an included file, this lists the included file name(s).
         */
        $xdebugAvailable = $useXdebug && \function_exists("xdebug_get_function_stack");
        $trace = \array_reverse(\debug_backtrace()); // reverse to get EvalSource
        \array_pop($trace); // Remove this call (getCurrent)
        $xdebug_trace = $xdebugAvailable ? \xdebug_get_function_stack() : null;
        $callFrames = array();
        $lastSource = null;
        $lastEvalSource = null;

        foreach ($trace as $key => $call)
        {
            $args = isset($call["args"]) ? $call["args"] : array(); // eval anomaly
            $class = isset($call["class"]) ? $call["class"] : null; // closure anomaly
            $object = isset($call["object"]) ? $call["object"] : null; // closure anomaly
            $line = isset($call["line"]) ? $call["line"] : 0; // internal anomaly
            $function = $call["function"];

            // (X) : eval()'d code
            if (isset($call["file"]) && (\strpos($call["file"], "eval()'d code") !== false))
                $source = $lastEvalSource;
            else if (isset($call["file"]))
                $source = new FileSource($call["file"]);
            else
                $source = null;

            if (\strpos($function, "{closure}") !== false)
            {
                $closureHandle = $xdebugAvailable ?
                    self::getClosureHandleFromFunction($xdebug_trace[$key + 1]["file"], $lastEvalSource) : null;
                $closureScope = new ClosureScope($function, $class, $object);
            }

            if ($function === "eval")
            {
                $xdebugFrame = $xdebugAvailable ? $xdebug_trace[$key + 1] : array();
                $lastCallFrame = \end($callFrames) !== false ? \end($callFrames) : null;
                $func = null;
                try
                {
                    $func = \function_exists($function) ? new ReflectionFunction($function) :
                        new ReflectionMethod($class, $function);
                } catch (Exception $e)
                {
                }
                $args =
                    self::tryGetArgumentsForEval($xdebugAvailable, $xdebugFrame, $line, $lastCallFrame, $func, $source,
                                                 $closureHandle);
                $lastEvalSource = $args !== array() ? new EvalSource("<?php " . \reset($args)) : null;
            }

            if (\strpos($function, "{closure}") !== false)
                $callFrames[] = new ClosureCallFrame($closureScope, $args, $line, $source, $closureHandle);
            else if (isset($call["class"]) && ($call["type"] === "->"))
                $callFrames[] = new InstanceMethodCallFrame($object, $class, $function, $args, $line, $source);
            else if (isset($call["class"]) && ($call["type"] === "::"))
                $callFrames[] = new StaticMethodCallFrame($class, $function, $args, $line, $source);
            else
                $callFrames[] = new FunctionCallFrame($function, $args, $line, $source);
        }

        $callFrames = \array_reverse($callFrames); // back to original order

        return new StackTrace($callFrames);
    }

    /**
     * @param string          $closure
     * @param EvalSource|null $lastEvalSource
     * @return ClosureHandle|null
     */
    private static function getClosureHandleFromFunction($closure, $lastEvalSource)
    {
        if ((\strlen($closure) <= 9) || (\strpos($closure, "{closure:") === false))
            return null;
        // (X) : eval()'d code:X-X}
        $start = \strpos($closure, ") : eval()'d code:");
        if ($start === false)
        {
            $start = \strrpos($closure, ":") + 1;
            $source = new FileSource(\substr($closure, 9, $start - 1));
        }
        else
        {
            $start += 18;
            $source = $lastEvalSource;
        }
        $mid = \strpos($closure, "-", $start);
        $end = \strpos($closure, "}", $mid);

        return new ClosureHandle($source,
                                 \substr($closure, $start, $mid - $start),
                                 \substr($closure, $mid + 1,
                                         $end - $mid - 1));
    }

    /**
     * @param bool                            $useXdebug
     * @param array                           $xdebugFrame
     * @param int                             $line
     * @param CallFrame|null                  $lastCallFrame
     * @param ReflectionFunctionAbstract|null $func
     * @param Source|null                     $source
     * @param ClosureHandle|null              $closureHandle
     * @return \mixed[]
     */
    private static function tryGetArgumentsForEval($useXdebug, array $xdebugFrame, $line,
        CallFrame $lastCallFrame = null, ReflectionFunctionAbstract $func = null, Source $source = null,
        ClosureHandle $closureHandle = null)
    {
        $args = array();
        if ($useXdebug)
        {
            $content = $xdebugFrame["include_filename"];
            $args = array($content);
        }
        else if ($source !== null)
        {
            $startLine = $func !== null ? $func->getStartLine() : 0;
            $endLine = $func !== null ? $func->getEndLine() : 0;
            $name = null;
            if (($line > 0) && ($source instanceof FileSource) && ($startLine > 0) && ($endLine > 0))
                $name = self::tryGetEvalArgFromSource($source, $startLine, $endLine, $line);
            else if (($line > 0) && ($source instanceof FileSource) && ($closureHandle !== null))
                $name = self::tryGetEvalArgFromSource($closureHandle->getSource(), $closureHandle->getStartLine(),
                                                      $closureHandle->getEndLine(), $line);
            else if ($line > 0)
                $name = self::tryGetEvalArgFromSource($source, 0, 0, $line);
            $args = ($name !== null) && $lastCallFrame->hasArgumentName($name) ?
                array($lastCallFrame->getArgumentByName($name)) : array();
        }

        return $args;
    }

    /**
     * @param Source $source
     * @param int    $startLine
     * @param int    $endLine
     * @param int    $evalLine
     * @return string|null
     */
    private static function tryGetEvalArgFromSource(Source $source, $startLine, $endLine, $evalLine)
    {
        if (($startLine > 0) && ($endLine > 0) && ($endLine >= $startLine) && ($evalLine > $startLine) &&
            ($evalLine < $endLine)
        )
        {
            // Cut content for speed
            $content = \implode('\n', \array_slice($source->getContentLines(), $startLine, $endLine - $startLine));
            $line = $evalLine - $startLine;
        }
        else
        {
            $content = $source->getContent();
            $line = $evalLine;
        }
        $content = \preg_replace('/\h+/', '', $content); // Remove all horizontal whitespace characters
        $matches = array();
        \preg_match_all('/eval\(\$([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)\)/m', $content, $matches,
                        PREG_OFFSET_CAPTURE | PREG_PATTERN_ORDER);

        // Search for last eval call until reaching $line
        $argName = null;
        $lastMatchLine = 0;
        foreach ($matches[1] as $match)
        {
            $before = \substr($content, 0, $match[1]); // Fetches all the text before the match
            $matchLine = \strlen($before) - \strlen(\str_replace("\n", "", $before)) + 1;
            if (($matchLine <= $line) && ($matchLine > $lastMatchLine))
            {
                $lastMatchLine = $matchLine;
                $argName = $match[0];
                $lastOffset = $match[1];
            }
            else if ($matchLine > $line)
                break;
        }

        // Look if argument is assigned before use
        $before = \substr($content, 0, $lastOffset);
        if (\preg_match('/\$' . $argName . '\R*=[^=]/m', $before) === 1)
            $argName = null; // Not usable, possibly changed

        return $argName;
    }

    /**
     * @param int $pos
     * @return CallFrame|null
     */
    public function getCallFrame($pos)
    {
        return isset($this->callFrames[$pos]) ? $this->callFrames[$pos] : null;
    }

    /**
     * @return CallFrame
     */
    public function getMostRecentCallFrame()
    {
        return \reset($this->callFrames);
    }

    /**
     * @return CallFrame
     */
    public function getLeastRecentCallFrame()
    {
        return \end($this->callFrames);
    }

    /**
     * @return CallFrame[]
     */
    public function getCallFrames()
    {
        return $this->callFrames;
    }

}
