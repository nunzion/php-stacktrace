<?php

namespace Nunzion\StackTrace;

abstract class Source
{
    /**
     * @return string[]
     */
    public function getContentLines()
    {
        return \explode("\n", $this->getContent());
    }

    /**
     * @return string
     */
    public abstract function getContent();
}
